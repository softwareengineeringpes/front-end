import Vue from 'vue'
import VueResource from 'vue-resource'

Vue.use(VueResource)

import config from './config'

export default {
    signUp ( state , callback ) {
        Vue.http.get(config.baseURL + config.path.signUp, {
            params: {
                email: state.signup.email,
                mobile: state.signup.mobile,
                password: state.signup.password
            }
        }).then(
            response => {
                callback(response.body)
            },
            response => {
            }
        )
    },
    logIn ( state, callback ) {
        Vue.http.get(config.baseURL + config.path.authenticate, {
            params: {
                email: state.login.email,
                password: state.login.password
            }
        }).then(
            response => {
                callback( response.body )
            },
            response => {
            }
        )
    }
}