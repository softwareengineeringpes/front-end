export default {
    baseURL: 'http://flynow.getspang.co/api',
    path: {
        signUp: '/users/create',
        authenticate: '/users/authenticate',
        suggest: {
            cities: '/suggest/cities'
        },
        searchFlights : '/search'
    },
    apis: {
        makemytrip: "http://flights.makemytrip.com/makemytrip/search-api.json"
    }
}