import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Flights from '@/components/Flights'
import BookFlight from '@/components/BookFlight'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/flights',
      name: 'Flights',
      component: Flights
    },
    {
      path: '/book/:flightNo',
      name: 'BookFlight',
      component: BookFlight
    }
  ],
  history: true
})
