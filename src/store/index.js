import Vue from 'vue'
import Vuex from 'vuex'
import API from '../api'

Vue.use(Vuex)

const state = {
    login: {
        email: null,
        password: null,
        status: false
    },
    signup: {
        email: null,
        mobile: null,
        password: null,
        status: false
    },
    userId: null,
    flightSearch: {
        fromCity: "Bangalore",
        fromCityCode: "BLR",
        toCityCode: "BOM",
        toCity: "Mumbai",
        classType: "E",
        departureDate: new Date(),
        result: null/*{
            filterData: {
                carrierList: {
                    "6E": {
                        originalCarrierCd: "G8",
                        carrierCode: "G8",
                        fare: 4526
                    }
                }
            },
            flights: [/*
                {
                    le: [{
                            an: "Indigo",
                            duration: "4h 40m",
                            noOfStops: "1 Stop",
                            cc: "6E",
                            fmtDepartureTime: "14:35",
                            fmtArrivalTime: "19:15",
                            o: "BLR",
                            d: "BOM"
                        }
                    ]
                }
            ]
        }*/
    }
}

const getters = {
    
}

const mutations = {
    loginEmail ( state, email ) {
        state.login.email = email
    },
    loginPassword ( state, password ) {
        state.login.password = password
    },
    signupEmail ( state, email ) {
        state.signup.email = email
    },
    signupMobile ( state, mobile ) {
        state.signup.mobile = mobile
    },
    signupPassword ( state, password ) {
        state.signup.password = password
    },
    signupStatus ( state ) {
        state.signup.status = !state.signup.status
    },
    loginStatus ( state ) {
        state.login.status = !state.login.status
    },
    userId ( state, userId ) {
        state.userId = userId
    },
    fromCity ( state, city ) {
        state.flightSearch.fromCity = city
    },
    fromCityCode ( state, code ) {
        state.flightSearch.fromCityCode = code
    },
    toCity ( state, city ) {
        state.flightSearch.toCity = city
    },
    toCityCode ( state, code ) {
        state.flightSearch.toCityCode = code
    },
    classType ( state, classType ) {
        state.flightSearch.classType = classType
    },
    departureDate ( state, departureDate ) {
        state.flightSearch.departureDate = departureDate
    },
    searchResult ( state, result ) {
        state.flightSearch.result = result
    }
}

const actions = {
    signUp ( context ) {
        API.signUp(context.state, function( data ) {
            if ( data.success ) {
                context.commit('signupStatus')
            }
        })
    },
    logIn ( context) {
        API.logIn(context.state, function( data ) {
            if ( data.success ) {
                context.commit('userId', data.userId)
                context.commit('loginStatus')
            }
        })
    }
}

export default new Vuex.Store({
    state,
    getters,
    mutations,
    actions
})